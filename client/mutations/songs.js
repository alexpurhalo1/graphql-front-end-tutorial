import gql from 'graphql-tag'

const createSong = gql`
	mutation AddSong($title: String) {
		addSong(title: $title) {
			id
			title
		}
	}
`;

const deleteSong = gql`
	mutation DeleteSong($id: ID) {
		deleteSong(id: $id) {
		 id 
		}
	}
`;

export { createSong, deleteSong }
