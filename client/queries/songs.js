import gql from 'graphql-tag'

const fetchSongs = gql`
	{
		songs {
			id
			title
		}
	}
`;

const fetchSong = gql`
	query FindSong($id: ID!) {
		song(id: $id) {
 			id
    	title
    	lyrics {
      	id
      	likes
      	content
    	}
 		}
	}
`;

export { fetchSongs, fetchSong }
