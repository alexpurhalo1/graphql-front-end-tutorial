import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { likeLyric } from '../mutations/lyrics';

const onLike = (id, likes, mutate) => (
	mutate({
		variables: { id },
		optimisticResponse: {
			__typeName: 'Mutation',
			likeLyric: {
				id,
				__typename: 'LyricType',
				likes: likes + 1
			}
		}
	})
);

const LyricList = ({ lyrics, mutate }) => (
	<ul className="collection">
		{lyrics.map(({id, content, likes}) =>
			<li key={id} className="collection-item">
				{content}
				<div className="vote-box">
					<i
						className="material-icons"
						onClick={() => onLike(id, likes, mutate)}
					>
						thumb_up
					</i>
					{likes}
				</div>
			</li>
		)}
	</ul>
);

export default graphql(likeLyric)(LyricList);
