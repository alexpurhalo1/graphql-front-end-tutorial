import React from 'react';
import { graphql } from 'react-apollo';
import { Link } from 'react-router';
import { fetchSongs } from '../queries/songs';
import { deleteSong } from '../mutations/songs';

const SongList = ({ data: { loading, songs, refetch }, mutate }) => (
	<div>
		<ul className="collection">
			{loading ? 'loading...' : songs.map(({ id, title }) =>
				<li key={id} className="collection-item">
					<Link to={`/songs/${id}`}>{title}</Link>
					<i
						className="material-icons"
						onClick={() => mutate({ variables: { id } }).then(() => refetch())}
						// onClick={() => mutate({ variables: { id} }, { refetchQueries: deleteSong })}
					>
						delete
					</i>
				</li>
			)}
		</ul>
		<Link
			to="/songs/new"
			className="btn-floating btn-large red right">
			<i className="material-icons">add</i>
		</Link>
	</div>
);

export default graphql(deleteSong)(
	graphql(fetchSongs)(SongList)
);

